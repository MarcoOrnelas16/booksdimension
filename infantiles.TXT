Alicia sinopsis:La historia cuenta cómo una niña llamada Alicia cae por un agujero, encontrándose en un mundo peculiar y extraño , poblado por humanos y criaturas antropomórficas. El libro juega con la lógica, dando a la novela gran popularidad tanto en niños como en adultos.
autor:Lewis Carroll
bibliografia:Charles Lutwidge Dodgson (Daresbury, Cheshire, Reino Unido; 27 de enero de 1832-Guildford, Surrey, Reino Unido; 14 de enero de 1898), más conocido por su seudónimo Lewis Carroll, fue un diácono anglicano, lógico, matemático, fotógrafo y escritor británico. Sus obras más conocidas son Alicia en el país de las maravillas y su continuación, A través del espejo y lo que Alicia encontró allí.


el principito sinopsis:El principito es una narración corta del escritor francés Antoine de Saint-Exupéry, que trata de la historia de un pequeño príncipe que parte de su asteroide a una travesía por el universo, en la cual descubre la extraña forma en que los adultos ven la vida y comprende el valor del amor y la amistad.
autor: Antoine de Saint-Exupéry
bibliografia:Antoine Marie Jean-Baptiste Roger Conde de Saint-Exupéry1​ (Lyon, 29 de junio de 1900 - Mar Mediterráneo, cerca de Marsella, 31 de julio de 1944), conocido como Antoine de Saint-Exupéry, fue un aviador y escritor francés, autor de la famosa obra El principito

caperucita sinopsis:Caperucita Roja es una niña que quería mucho a su abuelita; un día su madre le da una cesta con comida para que se la lleve a la abuelita, que está enferma y vive en una casa algo lejos de ellas. ​ En el camino se encuentra con el Lobo Feroz que la reta a una carrera hasta la casa de la abuelita.
autor: Charles Perrault 
bibliografia:Charles Perrault (París; 12 de enero de 1628 – ibídem; 16 de mayo de 1703) fue un escritor francés, principalmente reconocido por haber dado forma literaria a cuentos clásicos infantiles como Piel de asno, Pulgarcito, Barba Azul, La Cenicienta, La bella durmiente, Caperucita Roja y El Gato con Botas, atemperando en muchos casos la crudeza de las versiones orales, la mayoría de sus cuentos son infantiles y de fantasía.